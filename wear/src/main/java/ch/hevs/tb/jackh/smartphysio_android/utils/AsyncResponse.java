package ch.hevs.tb.jackh.smartphysio_android.utils;

public interface AsyncResponse {
    void processFinish(String result);
}