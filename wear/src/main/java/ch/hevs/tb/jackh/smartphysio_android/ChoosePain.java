package ch.hevs.tb.jackh.smartphysio_android;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.wear.widget.WearableLinearLayoutManager;
import android.support.wear.widget.WearableRecyclerView;
import android.support.wearable.activity.WearableActivity;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import ch.hevs.tb.jackh.smartphysio_android.adapters.PainAdapter;
import ch.hevs.tb.jackh.smartphysio_android.models.PainUI;
import ch.hevs.tb.jackh.smartphysio_android.utils.CustomScrollingLayoutCallback;
import ch.hevs.tb.jackh.smartphysio_android.utils.RecyclerItemClickListener;
import ch.hevs.tb.jackh.smartphysiolibrary.models.Credentials;
import ch.hevs.tb.jackh.smartphysiolibrary.models.PryvPain;

public class ChoosePain extends WearableActivity {

    private static final String TAG = ChoosePain.class.getName();

    private Credentials credentials;

    private WearableRecyclerView mRecyclerView;
    private List<PainUI> painUIList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_pain);


        findById();

        Configuration config = getResources().getConfiguration();
        if (config.isScreenRound()) {
            CustomScrollingLayoutCallback customScrollingLayoutCallback = new CustomScrollingLayoutCallback();
            mRecyclerView.setLayoutManager(new WearableLinearLayoutManager(getApplicationContext(), customScrollingLayoutCallback));

//            mRecyclerView.setEdgeItemsCenteringEnabled(true);
//            mRecyclerView.setCircularScrollingGestureEnabled(true);
        } else {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);//or HORIZONTAL
            mRecyclerView.setLayoutManager(linearLayoutManager);
        }


        populatePainLevelList();
        loadAdapter();

        credentials = new Credentials(this);
        if (credentials.hasCredentials()) {

        }


        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(ChoosePain.this, mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        //TODO : Vérifier connexion interne et si non connecté sauver en local le JSON

                        PryvPain pryvPain = new PryvPain(painUIList.get(position).getLevel());

                        Intent i = new Intent(ChoosePain.this, ReadyToSave.class);
                        i.putExtra("pryvPain", pryvPain);
                        startActivity(i);


                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                })
        );
    }

    /**
     * Intialize Activity
     * Poulate list pain
     * loading adapter
     * Find everything by Id
     */
    private void populatePainLevelList() {

        // specify an adapter (see also next example)
        String painScale[] = this.getResources().getStringArray(R.array.pain_level);
        String painDescription[] = this.getResources().getStringArray(R.array.pain_description);
        int colors[] = this.getResources().getIntArray(R.array.pain_color);

        for (int i = 0; i < painScale.length; i++) {
            int color = colors[i];
            PainUI painUI = new PainUI();
            painUI.setLevel(Integer.parseInt(painScale[i]));
            painUI.setDescription(painDescription[i]);
            painUI.setColor(color);
            painUIList.add(painUI);
        }
    }

    private void loadAdapter() {

        PainAdapter adapter = new PainAdapter(painUIList);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void findById() {

        mRecyclerView = findViewById(R.id.pain_level_recycler_view);
    }
}
