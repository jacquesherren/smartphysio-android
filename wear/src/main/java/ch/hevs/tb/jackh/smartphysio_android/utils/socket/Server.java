package ch.hevs.tb.jackh.smartphysio_android.utils.socket;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;

public class Server {

    private static final String TAG = "Server";

    public static void startServer(InetAddress inetAddress, int timeout, Context context){

        try {
            // Open socket for 30 seconds

            Log.i(TAG,"inetAddress :" + inetAddress);
            ServerSocket serverSocket = new ServerSocket();
            //Warning : the backlog value (2nd parameter is handled by the implementation
            //serverSocket = new ServerSocket(); // <-- create an unbound socket first
            serverSocket.setReuseAddress(true);
            serverSocket.bind(new InetSocketAddress(inetAddress.getHostAddress(), Global.PORT), 10);
            Log.i(TAG,"Before ServerSocket");
            //Warning : the backlog value (2nd parameter is handled by the implementation
            //serverSocket = new ServerSocket(Constants.PORT,10,inetAddress);


            //serverSocket.bind(new InetSocketAddress(localAddress, PORT),10);
            Log.i(TAG,"After ServerSocket");
            //serverSocket = new ServerSocket(PORT,10,localAddress);
            serverSocket.setSoTimeout(timeout);
            /*Log.i(TAG,"Default Timeout :" + serverSocket.getSoTimeout());
            Log.i(TAG,"Used IpAddress :" + serverSocket.getInetAddress());
            Log.i(TAG,"Listening on Port :" + serverSocket.getLocalPort());*/

            Socket clientSocket;
            //while(true) {
                Log.i(TAG,"Server ready on : " + serverSocket.toString() );
                clientSocket = serverSocket.accept();
                if(clientSocket!=null) {

                    Thread service = new Thread(new Service(clientSocket, context));
                    service.start();

                }
            //}

            Log.i(TAG,"==== Connection Timeout");



        } catch (IOException ioe) {
            Log.e(TAG,"IOException : " + ioe);

        } catch (Exception e) {
            Log.e(TAG,"Unkown error : " + e);
            System.exit(1);
        }

    }
}
