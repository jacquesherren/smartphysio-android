package ch.hevs.tb.jackh.smartphysio_android.utils.socket;

import android.content.Context;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

import ch.hevs.tb.jackh.smartphysiolibrary.models.Credentials;

public class Service implements Runnable {

    private Socket socket;
    private static final String TAG = "Service";
    private InputStream is;
    private OutputStream os;
    private String mUri;
    private Context mContext;

    protected Service(Socket socket, Context context){

        this.socket = socket;
        this.mContext = context;
    }

    @Override
    public void run() {
        try {
            this.is = socket.getInputStream();
            this.os = socket.getOutputStream();
            readInputHeaders();
            readURI();
            writeResponse("<html><body><h1>Hello</h1></body></html>");


        } catch (Throwable t) {
            /*do nothing*/
        } finally {
            try {
                socket.close();
            } catch (Throwable t) {

            }
        }
        System.out.println("Client processing finished");

    }

    private void writeResponse(String s) throws Throwable {
        String response = "HTTP/1.1 200 OK\r\n" +
                "Server: Server\r\n" +
                "Access-Control-Allow-Origin:http://localhost:3000"  + "\r\n" +
                "Access-Control-Allow-Credentials:true"  + "\r\n" +
                "Content-Type: text/html\r\n" +
                "Content-Length: " + s.length() + "\r\n" +
                "Connection: close\r\n\r\n";
        String result = response + s;
        os.write(result.getBytes());
        os.flush();
    }

    private void readInputHeaders() throws Throwable {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        mUri = br.readLine();
        while(true) {
            String s = br.readLine();
            System.out.println(s);
            if(s == null || s.trim().length() == 0) {
                break;
            }
        }
    }

    private void readURI(){
        System.out.println("URI - GET INFORMATION:");
        System.out.println("URI " + mUri);
        String origin = mUri;
        String username =origin.substring(origin.indexOf("?username=") + 10 , origin.indexOf("&token="));
        String token =origin.substring(origin.indexOf("&token=") + 7 , origin.indexOf("&EIO=3&transport"));

        System.out.println("username = " + username);
        System.out.println("token = " + token);

        Credentials credentials = new Credentials(mContext);
        credentials.setCredentials(username, token);

    }
}
