package ch.hevs.tb.jackh.smartphysio_android.utils;

import android.support.v7.widget.RecyclerView;
import android.support.wear.widget.WearableLinearLayoutManager;
import android.view.View;

public class CustomScrollingLayoutCallback extends WearableLinearLayoutManager.LayoutCallback {
    /**
     * How much should we scale the icon at most.
     */
    private static final float MAX_ICON_PROGRESS = 0.85f;

    private float mProgressToCenter;

    @Override
    public void onLayoutFinished(View child, RecyclerView parent) {


        // Figure out % progress from top to bottom
        float centerOffset = ((float) child.getHeight() / 1.5f) / (float) parent.getHeight();
        float yRelativeToCenterOffset = (child.getY() / parent.getHeight()) + centerOffset;
        // Normalize for center
        mProgressToCenter = Math.abs(0.5f - yRelativeToCenterOffset);
//        // Adjust to the maximum scale
        mProgressToCenter = Math.min(mProgressToCenter, MAX_ICON_PROGRESS);

        child.setScaleX(1 - mProgressToCenter);
        child.setScaleY(1 - mProgressToCenter);

//        mProgressToCenter = (float) Math.sin(yRelativeToCenterOffset * Math.PI);
//
        //     child.setScaleX(mProgressToCenter);
        //       child.setScaleY(mProgressToCenter);

    }
}

