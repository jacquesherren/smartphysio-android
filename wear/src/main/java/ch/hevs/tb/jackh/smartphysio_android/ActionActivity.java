package ch.hevs.tb.jackh.smartphysio_android;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.wear.widget.drawer.WearableActionDrawerView;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pryv.model.Event;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ch.hevs.tb.jackh.smartphysio_android.adapters.activity.ActivityAdapterHeader;
import ch.hevs.tb.jackh.smartphysiolibrary.models.ClientData;
import ch.hevs.tb.jackh.smartphysiolibrary.models.Credentials;
import ch.hevs.tb.jackh.smartphysiolibrary.models.FavoriteActivities;
import ch.hevs.tb.jackh.smartphysiolibrary.models.PryvActivity;
import ch.hevs.tb.jackh.smartphysiolibrary.models.PryvPain;
import ch.hevs.tb.jackh.smartphysiolibrary.models.PryvStream;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.AppConnected;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;


public class ActionActivity extends WearableActivity {

    private static final String TAG = MainActivity.class.getName();

    private List<Event> mActivitiesFavorites;

    private List<PryvStream> mStructuredActivitiesFavorites = new ArrayList<>();
    private List<PryvStream> mStructuredActivitiesOnline = new ArrayList<>();


    private RecyclerView mRecyclerViewHeader;

    private Credentials credentials;

    private WearableActionDrawerView mWearableActionDrawerView;
    private TextView mNoActivitiesAvailableTextview;
    private ImageButton mBackButton;

    private PryvPain mPryvPain;

    private boolean isChecked = false;
    private boolean isConnected = false;
    private boolean hasFavorites = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action);
        findById();

        // Get pain from previous activity
        this.mPryvPain = getIntent().getParcelableExtra("pryvPain");
        if (this.mPryvPain == null) {
            finish();
        }

        // Check credentials
        credentials = new Credentials(this);
        if (!credentials.hasCredentials()) {
            closeActivity();
        }

        // set RecyclerView
        mRecyclerViewHeader.setHasFixedSize(true);
        mRecyclerViewHeader.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewHeader.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        // Populate Action Drawer Menu
        Menu menu = mWearableActionDrawerView.getMenu();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_menu, menu);
        MenuItem sw = menu.findItem(R.id.action_switch_activity);

        structureActivitiesFavorites();

        if (AppConnected.connected && Global.activitiesOnline != null) {
            isConnected = true;
            mWearableActionDrawerView.getController().peekDrawer();
            structureActivitiesOnline();
        } else {
            Log.w(TAG, "Oooops non internet connection !!!");
        }

        // Check if patient has favorites activity in storage local
        if (mStructuredActivitiesFavorites.size() > 0) {
            hasFavorites = true;
            loadAdapter(mStructuredActivitiesFavorites);
        } else
            hasFavorites = false;


        setUI();

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity();
            }
        });

        sw.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (isChecked) { // Only favorites are displayed

                    sw.setIcon(R.drawable.ic_more_vert_24dp_wht);
                    loadAdapter(mStructuredActivitiesFavorites);
                } else { // Favorites and online activities are displayed

                    sw.setIcon(R.drawable.ic_favorite_white_24dp);
                    loadAdapter(mStructuredActivitiesOnline);
                }
                isChecked = !isChecked;
                setUI();
                return isChecked;

            }
        });

        //FavoriteActivities.resetFavorites(getApplicationContext());

    }


    private void setUI() {
        if (!isChecked) {
            if (isConnected) {
                if (hasFavorites) {
                    mNoActivitiesAvailableTextview.setVisibility(View.GONE);
                    mBackButton.setVisibility(View.GONE);
                } else {
                    mNoActivitiesAvailableTextview.setVisibility(View.VISIBLE);
                    mBackButton.setVisibility(View.VISIBLE);
                    mNoActivitiesAvailableTextview.setText(R.string.no_activities_founded_textview);
                }
            } else {

                if (hasFavorites) {
                    mNoActivitiesAvailableTextview.setVisibility(View.GONE);
                    mBackButton.setVisibility(View.GONE);
                } else {
                    mNoActivitiesAvailableTextview.setVisibility(View.VISIBLE);
                    mBackButton.setVisibility(View.VISIBLE);
                    mNoActivitiesAvailableTextview.setText(R.string.no_activities_and_no_connection_textview);
                }

            }
        } else {
            mNoActivitiesAvailableTextview.setVisibility(View.GONE);
            mBackButton.setVisibility(View.GONE);


        }

    }


    private void loadAdapter(List<PryvStream> activities) {


        ActivityAdapterHeader adapter = new ActivityAdapterHeader(getApplicationContext(), activities, mPryvPain);
        mRecyclerViewHeader.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }


    private void structureActivitiesFavorites() {

        if (!loadActivitiesFavorites()) {
            //Toast.makeText(ActionActivity.this, "you have no favorites...", Toast.LENGTH_SHORT).show();
        } else {
            PryvStream stream = new PryvStream(Global.FAVORITES_STREAM);
            mStructuredActivitiesFavorites = new ArrayList<>();
            mStructuredActivitiesFavorites.add(stream);
            mStructuredActivitiesFavorites.get(0).setEvents(mActivitiesFavorites);
        }
    }

    private void structureActivitiesOnline() {

        if (Global.activitiesOnline != null && Global.activitiesOnline.size() > 0) {
            mStructuredActivitiesOnline = new ArrayList<>();
            //       if(mStructuredActivitiesFavorites.size()==1) { mStructuredActivitiesOnline.add(mStructuredActivitiesFavorites.get(0)); }
            int indexFavorites = -1;
            for (int i = 0; i < Global.types.size(); i++) {
                if (Global.types.get(i).getId().equals(Global.FAVORITES_STREAM.getId()))
                    indexFavorites = i;
                mStructuredActivitiesOnline.add(Global.types.get(i));
                mStructuredActivitiesOnline.get(i).clearEvents();
                for (Event event : Global.activitiesOnline) {
                    if (event.getStreamId().equals(Global.types.get(i).getId())) {
                        mStructuredActivitiesOnline.get(i).addEvent(event);
                    }
                }
            }
            // remove favorites for online data list
            mStructuredActivitiesOnline.remove(indexFavorites);

        }
    }

    private boolean loadActivitiesFavorites() {

        Map<String, ClientData> favorites = FavoriteActivities.loadFavorites(getApplicationContext());
        if (favorites != null) {
            mActivitiesFavorites = new ArrayList<>();
            for (Map.Entry<String, ClientData> entry : favorites.entrySet()) {
                PryvActivity pryvActivity = new PryvActivity(entry.getValue().getId(), entry.getValue().getActivity(), entry.getValue().getStreamId());
                mActivitiesFavorites.add(pryvActivity);
            }
            return true;
        }

        return false;
    }

    private void closeActivity() {
        finish();
    }

    private void findById() {
        this.mRecyclerViewHeader = findViewById(R.id.activity_list);
        this.mWearableActionDrawerView = findViewById(R.id.action_drawer);
        this.mNoActivitiesAvailableTextview = findViewById(R.id.no_activities_founded_textview);
        this.mBackButton = findViewById(R.id.action_back_button);

    }
}
