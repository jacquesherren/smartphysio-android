package ch.hevs.tb.jackh.smartphysio_android.adapters.activity;

import android.view.View;

import com.pryv.model.Event;

public interface RecyclerViewClickListener {

    void onClick(View view, int position, Event event);

}
