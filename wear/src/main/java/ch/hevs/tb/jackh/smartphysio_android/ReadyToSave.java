package ch.hevs.tb.jackh.smartphysio_android;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.pryv.Connection;
import com.pryv.exceptions.ApiException;
import com.pryv.model.Event;

import java.io.IOException;

import ch.hevs.tb.jackh.smartphysiolibrary.models.ClientData;
import ch.hevs.tb.jackh.smartphysiolibrary.models.Credentials;
import ch.hevs.tb.jackh.smartphysiolibrary.models.PryvPain;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;

public class ReadyToSave extends WearableActivity {

    private static final String TAG = MainActivity.class.getName();

    private PryvPain pryvPain;
    private ImageButton mFlagAndSave;
    private ImageButton mSave;
    private ImageButton mChooseActivity;
    private ImageButton mBack;

    private ConstraintLayout mInitialLayout;
    private RelativeLayout mProgessLayout;

    private FrameLayout currentPainImage;

    private Credentials credentials;
    private Connection connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ready_to_save);


        this.pryvPain = getIntent().getParcelableExtra("pryvPain");
        if (this.pryvPain == null) {
            finish();
        }
        credentials = new Credentials(this);
        if (credentials.hasCredentials()) {
            connection = new Connection(credentials.getUsername(), credentials.getToken(), Global.DOMAIN);


        } else {
            closeActivity();
        }
        findById();

        final int value = Integer.parseInt(pryvPain.getContent().toString());


        switch (value) {
            case 0:
                currentPainImage.setBackgroundResource(R.drawable.ic_0);
                break;
            case 1:
                currentPainImage.setBackgroundResource(R.drawable.ic_1);
                break;
            case 2:
                currentPainImage.setBackgroundResource(R.drawable.ic_2);
                break;
            case 3:
                currentPainImage.setBackgroundResource(R.drawable.ic_3);
                break;
            case 4:
                currentPainImage.setBackgroundResource(R.drawable.ic_4);
                break;
            case 5:
                currentPainImage.setBackgroundResource(R.drawable.ic_5);
                break;
            case 6:
                currentPainImage.setBackgroundResource(R.drawable.ic_6);
                break;
            case 7:
                currentPainImage.setBackgroundResource(R.drawable.ic_7);
                break;
            case 8:
                currentPainImage.setBackgroundResource(R.drawable.ic_8);
                break;
            case 9:
                currentPainImage.setBackgroundResource(R.drawable.ic_9);
                break;
            case 10:
                currentPainImage.setBackgroundResource(R.drawable.ic_10);
                break;

        }

    }

    /**
     * Called when "Create a new note" button is clicked
     * Ask for the creation of a new note
     */
    public void addPain(View v, final PryvPain pryvPain) {
        if (credentials.hasCredentials()) {
            final Object painValue = pryvPain.getContent();
            Log.i(TAG, painValue.toString());
            final int value = 3;
            if (value >= 0 && value <= 10) {
                showProgress(true);
                new Thread() {
                    public void run() {
                        try {
                            Event newEvent = new Event()
                                    .setStreamId(pryvPain.getStreamId())
                                    .setType(pryvPain.getType())
                                    .setContent(pryvPain.getContent())
                                    .setCreated(pryvPain.getCreated())
                                    .setTime(pryvPain.getTime())
                                    .setClientData(pryvPain.getClientData());
                            newEvent = connection.events.create(newEvent);
                            updateStatusText("Saved"); // + newEvent.getId());
                        } catch (IOException | ApiException e) {
                            updateStatusText(e.toString());
                        }
                    }
                }.start();
            }
        }
    }


    private void updateStatusText(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ReadyToSave.this, text, Toast.LENGTH_SHORT).show();
                showProgress(false);
            }
        });
    }

    public void save(View v) {
        Toast.makeText(ReadyToSave.this, "Saving...", Toast.LENGTH_SHORT).show();
        addPain(v, pryvPain);
        closeActivity();
    }

    public void flagAndSave(View v) {
        ClientData clientData = new ClientData(true);
        this.pryvPain.addClientData(clientData);
        Toast.makeText(ReadyToSave.this, "Flagged and Saving...", Toast.LENGTH_SHORT).show();
        addPain(v, pryvPain);
        closeActivity();
    }

    public void back(View v) {
        //Toast.makeText(ReadyToSave.this, "Back", Toast.LENGTH_SHORT).show();
        closeActivity();
    }

    public void chooseActivity(View v) {
        Intent i = new Intent(ReadyToSave.this, ActionActivity.class);
        i.putExtra("pryvPain", pryvPain);
        startActivity(i);
    }

    private void findById() {

        this.mFlagAndSave = findViewById(R.id.flag_and_save_button);
        this.mSave = findViewById(R.id.save_button);
        this.mBack = findViewById(R.id.back_button);
        this.mChooseActivity = findViewById(R.id.choose_activity_button);

        this.mInitialLayout = findViewById(R.id.intial_layout);
        this.mProgessLayout = findViewById(R.id.progress_layout);

        this.currentPainImage = findViewById(R.id.currentPainImage_frameLayout);

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            System.out.println("shortAnimTime : " + shortAnimTime);
            mInitialLayout.setVisibility(show ? View.GONE : View.VISIBLE);
            mInitialLayout.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mInitialLayout.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgessLayout.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgessLayout.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgessLayout.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgessLayout.setVisibility(show ? View.VISIBLE : View.GONE);
            mInitialLayout.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void closeActivity() {
        finish();
    }
}
