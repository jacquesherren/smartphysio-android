package ch.hevs.tb.jackh.smartphysio_android.adapters;

import android.support.wear.widget.WearableRecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ch.hevs.tb.jackh.smartphysio_android.R;
import ch.hevs.tb.jackh.smartphysio_android.models.PainUI;
import ch.hevs.tb.jackh.smartphysio_android.utils.ItemSelectedListener;


/**
 * Created by jackh on 27.03.2018.
 */

public class PainAdapter extends WearableRecyclerView.Adapter<PainAdapter.MyViewHolder> {

    private static final String TAG = "PainAdapter";
    private List<PainUI> painUIList;
    private ItemSelectedListener itemSelectedListener;

    public PainAdapter(List<PainUI> painUIS) {
        this.painUIList = painUIS;
    }

    public void setListener(ItemSelectedListener itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pain_level_item, parent, false));

    }

    @Override
    public void onBindViewHolder(PainAdapter.MyViewHolder holder, int position) {
        Log.d(TAG, "Element " + position + " set.");


        if (painUIList != null && !painUIList.isEmpty()) {
            PainUI painUI = painUIList.get(position);
            holder.mPainDescriptionTextView.setText(painUI.getDescription());
            holder.mPainLevelTextView.setText(Integer.toString(painUI.getLevel()));
            switch (painUI.getLevel()) {
                case 0:
                    holder.mPainLevelTextView.setBackgroundResource(R.drawable.ic_circle_0);
                    break;
                case 1:
                case 2:
                case 3:
                    holder.mPainLevelTextView.setBackgroundResource(R.drawable.ic_circle_123);
                    break;
                case 4:
                case 5:
                case 6:
                    holder.mPainLevelTextView.setBackgroundResource(R.drawable.ic_circle_456);
                    break;
                case 7:
                case 8:
                case 9:
                    holder.mPainLevelTextView.setBackgroundResource(R.drawable.ic_circle_789);
                    break;
                case 10:
                    holder.mPainLevelTextView.setBackgroundResource(R.drawable.ic_circle_10);
                    break;
            }


            //holder.mPainLevelTextView.setTextColor(painUI.getColor());
            holder.bind(position, itemSelectedListener);
        }
    }



    @Override
    public int getItemCount() {
        if (painUIList != null && !painUIList.isEmpty()) {
            return painUIList.size();
        }
        return 0;
    }

    protected static class MyViewHolder extends WearableRecyclerView.ViewHolder {

        private final TextView mPainLevelTextView;
        private final TextView mPainDescriptionTextView;

        private MyViewHolder(final View itemView) {
            super(itemView);
            mPainLevelTextView = itemView.findViewById(R.id.pain_level_textView);
            mPainDescriptionTextView = itemView.findViewById(R.id.pain_description_textView);

        }

        private void bind(final int position, final ItemSelectedListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onItemSelected(position);

                    }
                }
            });
        }


    }
}
