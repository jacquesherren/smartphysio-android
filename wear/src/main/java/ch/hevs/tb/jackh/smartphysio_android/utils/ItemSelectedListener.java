package ch.hevs.tb.jackh.smartphysio_android.utils;

import android.view.View;

/**
 * Created by jackh on 28.03.2018.
 */

public interface ItemSelectedListener {
    void onItemSelected(int position);
    void onItemClick(View view, int position);
    void onLongItemClick(View view, int position);
}
