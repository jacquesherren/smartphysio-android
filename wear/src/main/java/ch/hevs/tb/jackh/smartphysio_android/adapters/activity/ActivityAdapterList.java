package ch.hevs.tb.jackh.smartphysio_android.adapters.activity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pryv.model.Event;

import java.util.List;

import ch.hevs.tb.jackh.smartphysio_android.R;

public class ActivityAdapterList extends RecyclerView.Adapter<ActivityAdapterList.ItemViewHolder> {

    private Context mContext;
    private List<Event> mActivities;

    private RecyclerViewClickListener mListener;

    public ActivityAdapterList(Context context, List<Event> activities, RecyclerViewClickListener listener) {
        this.mActivities = activities;
        this.mContext = context;
        this.mListener = listener;

    }

    @NonNull
    @Override
    public ActivityAdapterList.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.action_item, null);
        ItemViewHolder viewHolder = new ItemViewHolder(view, mListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityAdapterList.ItemViewHolder holder, int position) {
        final Event event = mActivities.get(position);

        holder.name.setText(event.getContent().toString());
        holder.event = event;
    }

    @Override
    public int getItemCount() {
        return (null != mActivities ? mActivities.size() : 0);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Event event;
        private TextView name;
        private LinearLayout layout;
        private RecyclerViewClickListener mListener;

        public ItemViewHolder(View view, RecyclerViewClickListener listener) {
            super(view);
            this.name = view.findViewById(R.id.activity_name_textView);
            this.layout = view.findViewById(R.id.layout_activity_list_item);
            this.mListener = listener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(view, getAdapterPosition(), event);
        }
    }
}
