package ch.hevs.tb.jackh.smartphysio_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ch.hevs.tb.jackh.smartphysiolibrary.models.Credentials;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.AppConnected;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.PryvApi;


public class MainActivity extends WearableActivity {

    private static final String TAG = MainActivity.class.getName();

    private Credentials credentials;

    private static final int LOGIN_REQUEST = 1;
    private TextView mWelcomeTextView;
    private ImageButton mSignInButton;
    private ImageButton mSignOutButton;
    private ImageButton mGoToChoosePainButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findById();

        credentials = new Credentials(this);
        if (credentials.hasCredentials()) {
            setLogoutView();
            Global.types = new ArrayList<>();
            Global.types.add(Global.FAVORITES_STREAM);
            if (AppConnected.getInstance(getApplicationContext()).isOnline()) {

                PryvApi.initPryvConnection(getApplicationContext(), credentials);
                PryvApi.getPublicActivitiesOnPryv(getApplicationContext());
                PryvApi.getPublicActivitiesTypesOnPryv(getApplicationContext());

            } else {
                /**
                 * Internet is NOT available, Toast It!
                 */
                Toast.makeText(getApplicationContext(), "Ooops! No WiFi/Mobile Networks Connected!", Toast.LENGTH_SHORT).show();
            }
            Intent intent = new Intent(this, ChoosePain.class);
            startActivity(intent);



        } else {
            setLoginView();
        }


        // Enables Always-on
        setAmbientEnabled();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (credentials.hasCredentials()) {
            // Navigate to Choose pain activity...
            setLogoutView();
        } else {
            setLoginView();
        }

    }


    private void setLoginView() {
        mWelcomeTextView.setText(R.string.please_singin_to_connect);

        // Hide sign out button & and manage sign in button
        mSignOutButton.setVisibility(View.GONE);
        mGoToChoosePainButton.setVisibility(View.GONE);
        mSignInButton.setVisibility(View.VISIBLE);
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLogin();
            }
        });
    }

    private void setLogoutView() {
        String welcome = getString(R.string.welcome_to_smartphysio, credentials.getUsername());
        mWelcomeTextView.setText(welcome);

        // Hide sign in button and manage sign out button
        mSignInButton.setVisibility(View.GONE);
        // Prod version without sign out button
        mSignOutButton.setVisibility(View.GONE);
        //mSignOutButton.setVisibility(View.VISIBLE);
        mGoToChoosePainButton.setVisibility(View.VISIBLE);
        mSignOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                credentials.resetCredentials();
                setLoginView();
            }
        });

        mGoToChoosePainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (credentials.hasCredentials()) {
                    Intent intent = new Intent(v.getContext(), ChoosePain.class);
                    startActivityForResult(intent, LOGIN_REQUEST);
                } else {
                    setLoginView();
                }
            }
        });
    }

    private void startLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, LOGIN_REQUEST);
        Log.i(TAG, "back from LoginActivity");
    }


    private void findById() {
        mWelcomeTextView = findViewById(R.id.welcome_textview);
        mSignInButton = findViewById(R.id.sign_in_button);
        mSignOutButton = findViewById(R.id.sign_out_button);
        mGoToChoosePainButton = findViewById(R.id.gotolist_button);
    }
}


