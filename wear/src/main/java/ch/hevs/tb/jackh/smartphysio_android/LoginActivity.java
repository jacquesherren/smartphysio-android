package ch.hevs.tb.jackh.smartphysio_android;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pryv.model.Permission;

import java.util.ArrayList;

import ch.hevs.tb.jackh.smartphysio_android.utils.AsyncResponse;
import ch.hevs.tb.jackh.smartphysiolibrary.models.Credentials;


public class LoginActivity extends WearableActivity implements AsyncResponse {

    private static final String TAG = LoginActivity.class.getName();

    SigninAsync signinAsync;

    private EditText mUsername;
    private EditText mToken;

    private TextView mTextViewConnexion;
    private ImageButton mButtonSignIn;
    private ImageButton mButtonCancel;

    private RelativeLayout mInitialLayout;
    private RelativeLayout mProgessLayout;


    private Permission creatorPermission = new Permission("*", com.pryv.model.Permission.Level.manage, "Creator");
    private ArrayList<Permission> permissions;

    private Context mContext;

    private Credentials credentials;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findById();


        credentials = new Credentials(this);
        credentials.resetCredentials();



        permissions = new ArrayList<>();
        permissions.add(creatorPermission);

        signinAsync = new SigninAsync();
        signinAsync.delegate = this;
        mButtonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext = v.getContext();

                signinAsync.execute();

            }
        });

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

    }


    private void findById() {
        this.mUsername = findViewById(R.id.username_edittext);
        this.mToken = findViewById(R.id.token_edittext);

        this.mTextViewConnexion = findViewById(R.id.connexion_textview);
        this.mButtonSignIn = findViewById(R.id.sign_in_button);
        this.mButtonCancel = findViewById(R.id.cancel_button);
        this.mInitialLayout = findViewById(R.id.intial_layout);
        this.mProgessLayout = findViewById(R.id.progress_layout);

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            System.out.println("shortAnimTime : " + shortAnimTime);
            mInitialLayout.setVisibility(show ? View.GONE : View.VISIBLE);
            mInitialLayout.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mInitialLayout.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgessLayout.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgessLayout.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgessLayout.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgessLayout.setVisibility(show ? View.VISIBLE : View.GONE);
            mInitialLayout.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void closeLoginActivity() {
        Log.i(TAG, "closeLoginActivity");
        //finishActivity(0);
        finish();
    }

    @Override
    public void processFinish(String result) {
        Log.i(TAG, "processFinish : " + result);
    }


    private class SigninAsync extends AsyncTask<String, Void, String> {
        private AsyncResponse delegate = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(true);
        }

        public SigninAsync() {

        }

        @Override
        protected String doInBackground(String... params) {
            String username = mUsername.getText().toString();
            String token = mToken.getText().toString();

            try {
                System.out.println("username = " + username);
                System.out.println("token = " + token);

                Credentials credentials = new Credentials(mContext);
                credentials.setCredentials(username, token);

                Log.i(TAG, "credentials : " + credentials.getUsername());

                return credentials.getUsername() + ";" + credentials.getToken();

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String finalResult) {
            super.onPostExecute(finalResult);
            if (!isCancelled()) {
                if (delegate != null) {
                    delegate.processFinish(finalResult);
                    showProgress(false);
                    closeLoginActivity();
                }
            }
        }
    }
}
