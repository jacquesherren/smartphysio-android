package ch.hevs.tb.jackh.smartphysio_android.adapters.activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pryv.model.Event;

import java.util.List;

import ch.hevs.tb.jackh.smartphysio_android.ChoosePain;
import ch.hevs.tb.jackh.smartphysio_android.R;
import ch.hevs.tb.jackh.smartphysiolibrary.models.ClientData;
import ch.hevs.tb.jackh.smartphysiolibrary.models.FavoriteActivities;
import ch.hevs.tb.jackh.smartphysiolibrary.models.PryvPain;
import ch.hevs.tb.jackh.smartphysiolibrary.models.PryvStream;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.PryvApi;


public class ActivityAdapterHeader extends RecyclerView.Adapter<ActivityAdapterHeader.MyViewHolder> {


    private List<PryvStream> mStructuredActivities;
    private Context mContext;

    private ImageButton moreButton;
    private boolean expanded = true;
    private ObjectAnimator animBT;
    private ObjectAnimator animRV;

    private RecyclerViewClickListener mListener;

    private PryvPain mPryvPain;


    public ActivityAdapterHeader(Context context, List<PryvStream> structuredActivities, PryvPain pryvPain) {

        this.mContext = context;
        this.mStructuredActivities = structuredActivities;
        this.mPryvPain = pryvPain;

    }

    @NonNull
    @Override
    public ActivityAdapterHeader.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (!(this.mStructuredActivities.isEmpty())) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.action_item_header, null);
            MyViewHolder viewHolder = new MyViewHolder(view);


            return viewHolder;

        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final ActivityAdapterHeader.MyViewHolder holder, int position) {

        final PryvStream pryvStream = mStructuredActivities.get(position);
        final String title = pryvStream.getName();
        final List<Event> childs = pryvStream.getEvents();
        final int countItem = countChildItems(childs);


        holder.titleTypes.setText(title);
        holder.countItem.setText(Integer.toString(countItem));

        holder.layout.setBackgroundResource(R.color.secondaryColor);

        RecyclerViewClickListener listener = (view, pos, event) -> {

            ClientData clientData = new ClientData(event.getId(), event.getContent().toString(), event.getStreamId());

            if (event.getCreated() != null)
                FavoriteActivities.addFavorite(mContext, clientData);

            mPryvPain.addClientData(clientData);
            Toast.makeText(mContext, "Saving...", Toast.LENGTH_SHORT).show();
            PryvApi.addPain(mContext, mPryvPain);
            Toast.makeText(mContext, "Saved", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(mContext, ChoosePain.class);
            mContext.startActivity(i);
        };

        ActivityAdapterList recyclerviewChilds = new ActivityAdapterList(mContext, childs, listener);
        holder.childs.setHasFixedSize(true);
        holder.childs.setAdapter(recyclerviewChilds);
        holder.childs.setNestedScrollingEnabled(false);
        holder.childs.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));

        holder.childs.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        //Si not data was found hide more button
        if (countItem > 0)
            holder.moreButton.setVisibility(View.VISIBLE);
        else
            holder.moreButton.setVisibility(View.INVISIBLE);

        holder.moreButton.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View v) {
                if (expanded) {
                    animRV = ObjectAnimator.ofInt(
                            holder.childs,
                            "visibility",
                            View.GONE);
                    animBT = ObjectAnimator.ofFloat(
                            v,
                            "rotation",
                            0,
                            180);
                    expanded = false;
                } else {
                    animRV = ObjectAnimator.ofInt(
                            holder.childs,
                            "visibility",
                            View.VISIBLE);
                    animBT = ObjectAnimator.ofFloat(
                            v,
                            "rotation",
                            180,
                            0);
                    expanded = true;
                }

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(animRV, animBT);
                animatorSet.setDuration(100);
                animatorSet.start();
            }
        });


    }

    private int countChildItems(List<Event> childs) {
        if (childs != null && childs.size() > 0)
            return childs.size();

        return 0;
    }

    @Override
    public int getItemCount() {
        return mStructuredActivities.size();
    }


    protected static class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView titleTypes;
        private final TextView countItem;
        private final RecyclerView childs;
        private final ImageButton moreButton;
        private final RelativeLayout layout;


        private MyViewHolder(View itemView) {
            super(itemView);
            this.titleTypes = itemView.findViewById(R.id.title_types_textview);
            this.countItem = itemView.findViewById(R.id.count_items_textview);
            this.childs = itemView.findViewById(R.id.childs_recycler_view);
            this.moreButton = itemView.findViewById(R.id.toggle_collapse_expend);
            this.layout = itemView.findViewById(R.id.header_activity_layout);
        }

    }
}