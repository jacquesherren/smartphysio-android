package ch.hevs.tb.jackh.smartphysio_android.models;

public class PainUI {


    private int level;
    private String description;
    private int color;

    public PainUI(int level) {
        this.level = level;
    }

    public PainUI() {

    }

    public PainUI(PainUI painUI) {
        this.level = painUI.getLevel();

        this.color = painUI.getColor();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
