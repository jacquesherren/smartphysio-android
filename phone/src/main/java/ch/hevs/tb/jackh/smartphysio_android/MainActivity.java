package ch.hevs.tb.jackh.smartphysio_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.pryv.Connection;
import com.pryv.exceptions.ApiException;
import com.pryv.model.Stream;

import java.io.IOException;

import ch.hevs.tb.jackh.smartphysiolibrary.models.Credentials;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();
    private static final int LOGIN_REQUEST = 1;
    private TextView progressView;
    private Credentials credentials;
    private Stream painStream;
    private Stream smartPhysioStream;

    private CardView mSignIn;


    private Connection connection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressView = findViewById(R.id.progress);
        mSignIn = findViewById(R.id.sign_in_button);

        mSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLogin();
            }
        });

    }

//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        super.onPrepareOptionsMenu(menu);
//        if (credentials.hasCredentials())
//            return true;
//
//        return false;
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        super.onCreateOptionsMenu(menu);
//        MenuInflater inflater = getMenuInflater();
//        //R.menu.menu est l'id de notre menu
//        inflater.inflate(R.menu.menu, menu);
//        return true;
//    }
//
//
//    public void onFilterGroupItemClick(MenuItem item) {
//        Toast.makeText(MainActivity.this, "click", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.sign_out_button_menu:
//                //Dans le Menu "m", on active tous les items dans le groupe d'identifiant "R.id.group2"
//                signOut();
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }


    @Override
    protected void onResume() {
        super.onResume();
        credentials = new Credentials(this);

        if (credentials.hasCredentials()) {
            initPryvConnection();
            setLogoutView();
        } else {
            setLoginView();
        }
    }

    /**
     * Create default stream structure
     */
    private void initPryvConnection() {
        // Initiate new connection to Pryv with connected account
        connection = new Connection(credentials.getUsername(), credentials.getToken(), Global.DOMAIN);

        // Initiate two streams : "Smartphysio" containing "Painslevel"
        painStream = new Stream()
                .setId(Global.PAIN_STREAM_ID)
                .setName(Global.PAIN_STREAM_NAME);

        smartPhysioStream = new Stream()
                .setId(Global.PAIN_STREAM_PARENT_ID)
                .setName(Global.PAIN_STREAM_PARENT_NAME)
                .addChildStream(painStream);

        // start a new thread
        new Thread() {
            public void run() {
                try {
                    Stream createdSmartPhysioStream = connection.streams.create(smartPhysioStream);
                    updateStatusText("Stream created with id: " + createdSmartPhysioStream.getId());
                    Stream createdPainStream = connection.streams.create(painStream);
                    updateStatusText("Stream created with id: " + createdPainStream.getId());
                } catch (IOException e) {
                    updateStatusText(e.toString());
                } catch (ApiException e) {
                    e.printStackTrace();
                    updateStatusText("painStream : " + e.toString());
                }
            }
        }.start();
    }

    private void updateStatusText(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setLoginView() {
        progressView.setText("Hello guest!");

    }

//    private void signOut() {
//        credentials.resetCredentials();
//        setLoginView();
//    }

    private void setLogoutView() {
        progressView.setText("Hello " + credentials.getUsername() + "!");

        Intent intent = new Intent(this, EventsActivity.class);
        startActivity(intent);
        ;
    }

    private void startLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, LOGIN_REQUEST);
    }
}
