package ch.hevs.tb.jackh.smartphysio_android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.highsoft.highcharts.Common.HIChartsClasses.HIOptions;
import com.highsoft.highcharts.Common.HIChartsClasses.HISpline;
import com.highsoft.highcharts.Core.HIChartView;
import com.pryv.Connection;
import com.pryv.exceptions.ApiException;
import com.pryv.model.Event;
import com.pryv.model.Filter;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import ch.hevs.tb.jackh.smartphysio_android.R;
import ch.hevs.tb.jackh.smartphysio_android.chart.HIChartManager;
import ch.hevs.tb.jackh.smartphysiolibrary.models.Credentials;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;

public class FragmentChart extends Fragment {

    private static FragmentChart mFragment;
    private HIChartView mSplineView;
    private HIOptions mHiOptions;
    private List<Event> mEventsHistory;
    private Number[][] mAllDatas;

    public static FragmentChart newInstance() {
        mFragment = new FragmentChart();
        return mFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEventsHistory = new ArrayList<>();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mEventsHistory.size() > 0)
            updateChart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chart_events_fragment, container, false);

        mSplineView = view.findViewById(R.id.spline);
        mSplineView.setWillNotDraw(true);

        retrievePains(this.getContext());

        return view;
    }

    private void retrievePains(final Context context) {
        Credentials credentials = new Credentials(context);
        if (credentials.hasCredentials()) {
            final Connection connection = new Connection(credentials.getUsername(), credentials.getToken(), Global.DOMAIN);
            new Thread() {
                public void run() {
                    try {
                        Filter filter = new Filter().addStream(Global.PAIN_STREAM);
                        filter.setLimit(0);
                        List<Event> events = connection.events.get(filter);


                        updateDatas(context, events);
                    } catch (IOException e) {
                        //updateStatusText(context, e.toString());
                    } catch (ApiException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    private void updateDatas(final Context context, final List<Event> events) {
        mFragment.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                events.sort(new Comparator<Event>() {
                    @Override
                    public int compare(Event e1, Event e2) {
                        //return e1.getCreated() > e2.getCreated() ? 1 :(e1.getCreated() < e2.getCreated() ? -1 : 0); //Sorted in Ascending
                        return e1.getCreated() > e2.getCreated() ? -1 : (e1.getCreated() < e2.getCreated() ? 1 : 0); // Descending
                    }
                });
                //mAdapter.notifyDataSetChanged();
                mEventsHistory = new ArrayList<>();
                mEventsHistory.clear();
                mEventsHistory.addAll(events);

                updateChart();
            }
        });
    }

    private void updateChart() {

        mAllDatas = new Number[mEventsHistory.size()][mEventsHistory.size()];
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        ArrayList<String> categories = new ArrayList<>();

        for (int i = 0; i < mEventsHistory.size(); i++) {

            Double d = mEventsHistory.get(i).getCreated();
            Timestamp timestamp = new Timestamp(d.longValue() * 1000L);
            Long l = new Long(d.longValue());

            String strDate = dateFormat.format(new Date(timestamp.getTime()));
            categories.add(strDate);
            mAllDatas[i][0] = l;
            mAllDatas[i][1] = Integer.valueOf(mEventsHistory.get(i).getContent().toString());
        }


        float height = getResources().getDisplayMetrics().heightPixels;

        final HISpline spline = HIChartManager.setSpline(mAllDatas);

        mHiOptions = new HIOptions();
        mHiOptions = HIChartManager.setChartOptions(categories);

        mHiOptions.setSeries(new ArrayList<>(Collections.singletonList(spline)));

        mSplineView.setOptions(mHiOptions);

        mSplineView.setWillNotDraw(false);
        mSplineView.getLayoutParams().height = Math.round(height * 5 / 8);


    }
}
