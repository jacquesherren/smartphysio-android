package ch.hevs.tb.jackh.smartphysio_android.chart;

import com.highsoft.highcharts.Common.HIChartsClasses.HIButtonOptions;
import com.highsoft.highcharts.Common.HIChartsClasses.HIChart;
import com.highsoft.highcharts.Common.HIChartsClasses.HICredits;
import com.highsoft.highcharts.Common.HIChartsClasses.HIExporting;
import com.highsoft.highcharts.Common.HIChartsClasses.HILabels;
import com.highsoft.highcharts.Common.HIChartsClasses.HINavigation;
import com.highsoft.highcharts.Common.HIChartsClasses.HIOptions;
import com.highsoft.highcharts.Common.HIChartsClasses.HIPlotOptions;
import com.highsoft.highcharts.Common.HIChartsClasses.HISpline;
import com.highsoft.highcharts.Common.HIChartsClasses.HIStyle;
import com.highsoft.highcharts.Common.HIChartsClasses.HITheme;
import com.highsoft.highcharts.Common.HIChartsClasses.HITitle;
import com.highsoft.highcharts.Common.HIChartsClasses.HITooltip;
import com.highsoft.highcharts.Common.HIChartsClasses.HIXAxis;
import com.highsoft.highcharts.Common.HIChartsClasses.HIYAxis;
import com.highsoft.highcharts.Common.HIColor;
import com.highsoft.highcharts.Common.HIGradient;
import com.highsoft.highcharts.Common.HIStop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class HIChartManager {

    public static HISpline setSpline(Number[][] allData) {

        final HISpline spline = new HISpline();
        spline.setTooltip(new HITooltip());
        spline.getTooltip().setHeaderFormat("");
        spline.getTooltip().setValueSuffix(" pains");
        spline.setShowInLegend(false);
        //spline.setColor(HIColor.initWithRGBA(186, 136, 0, 0.5));


        spline.setData(new ArrayList<>(Arrays.asList(allData)));


        return spline;
    }


    public static HIOptions setChartOptions(ArrayList<String> categories) {

        HIOptions hiOptions = new HIOptions();

        HIChart chart = new HIChart();
        chart.setType("spline");

        HIGradient gradient = new HIGradient(0, 0, 0, 1);
        LinkedList<HIStop> stops = new LinkedList<>();
        stops.add(new HIStop(0, HIColor.initWithRGB(255, 255, 255)));
        stops.add(new HIStop(1, HIColor.initWithRGB(222, 222, 222)));
        chart.setBackgroundColor(HIColor.initWithLinearGradient(gradient, stops));
        /*chart.setBorderColor(HIColor.initWithRGB(128, 128, 128));
        chart.setBorderWidth(2);*/
        chart.setBorderRadius(6);


        HINavigation navigation = new HINavigation();
        navigation.setButtonOptions(new HIButtonOptions());
        navigation.getButtonOptions().setSymbolStroke(HIColor.initWithRGBA(186, 136, 0, 0));
        HITheme theme = new HITheme();
        theme.setFill("rgba(0,0,0,0.0)");
        navigation.getButtonOptions().setTheme(theme);
        hiOptions.setNavigation(navigation);

        HIPlotOptions plotOptions = new HIPlotOptions();
        plotOptions.setSpline(new HISpline());
        plotOptions.getSpline().setColor(HIColor.initWithRGBA(186, 136, 0, 1));
        hiOptions.setPlotOptions(plotOptions);

        HICredits credits = new HICredits();
        credits.setEnabled(true);
        credits.setText("www.highcharts.com");
        HIStyle creditsStyle = new HIStyle();
        creditsStyle.setColor("rgba(255, 255, 255, 0.6)");
        credits.setStyle(creditsStyle);
        hiOptions.setCredits(credits);

        HITitle title = new HITitle();
        title.setText("History");
        title.setAlign("left");
        HIStyle titleStyle = new HIStyle();
        titleStyle.setFontFamily("Arial");
        titleStyle.setFontSize("14px");
        titleStyle.setColor("rgba(186, 136, 0, 0.6)");
        title.setStyle(titleStyle);
        title.setY(16);
        hiOptions.setTitle(title);

    /*    HISubtitle subtitle = new HISubtitle();
        subtitle.setText("Hisubtitle");

        subtitle.setAlign("left");
        HashMap<String, String> subtitleStyle = new HashMap<>();
        subtitleStyle.put("fontFamily", "Arial");
        subtitleStyle.put("fontSize", "10px");
        subtitleStyle.put("color", "rgba(255, 255, 255, 0.6)");
        subtitle.setStyle(subtitleStyle);
        subtitle.setY(28);
        hiOptions.setSubtitle(subtitle);*/

        HITooltip tooltip = new HITooltip();
        tooltip.setHeaderFormat("");
        hiOptions.setTooltip(tooltip);

        final HIXAxis xaxis = new HIXAxis();
        HITitle xTitle = new HITitle();
        xTitle.setText("time");
        xaxis.setTitle(new HITitle());
        xaxis.setTickColor(HIColor.initWithRGBA(128, 128, 128, 0.0));
        xaxis.setLineColor(HIColor.initWithRGBA(128, 128, 128, 0.3));
        xaxis.setLabels(new HILabels());
        HIStyle xLabelsStyle = new HIStyle();
        xLabelsStyle.setColor("rgb(128, 128, 128)");
        xLabelsStyle.setFontFamily("Arial");
        xLabelsStyle.setFontSize("10px");
        xaxis.getLabels().setStyle(xLabelsStyle);
        xaxis.getLabels().setStep(1.0);
        //xaxis.setCategories(categories);
        //xaxis.setCategories(new ArrayList<>(Arrays.asList(categories)));
        hiOptions.setXAxis(new ArrayList<HIXAxis>() {{
            add(xaxis);
        }});

        final HIExporting hiExporting = new HIExporting();
        hiExporting.setEnabled(false);
        hiOptions.setExporting(hiExporting);

        final HIYAxis yaxis = new HIYAxis();
        HITitle yTitle = new HITitle();
        yTitle.setText("pain level");
        yaxis.setLineWidth(1);
        yaxis.setGridLineWidth(0);
        yaxis.setLineColor(HIColor.initWithRGBA(128, 128, 128, 0.3));
        yaxis.setLabels(new HILabels());
        HIStyle yLabelsStyle = new HIStyle();
        yLabelsStyle.setColor("rgb(128, 128, 128)");
        yLabelsStyle.setFontFamily("Arial");
        yLabelsStyle.setFontSize("10px");
        yaxis.getLabels().setStyle(yLabelsStyle);
        yaxis.getLabels().setX(-5);
        yaxis.setTitle(new HITitle());
        yaxis.getTitle().setText("");
        yaxis.setCeiling(10);
        yaxis.setFloor(0);
        hiOptions.setYAxis(new ArrayList<HIYAxis>() {{
            add(yaxis);
        }});

        hiOptions.setChart(chart);

        return hiOptions;
    }
}
