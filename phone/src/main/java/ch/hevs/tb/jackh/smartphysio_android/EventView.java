package ch.hevs.tb.jackh.smartphysio_android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import ch.hevs.tb.jackh.smartphysiolibrary.models.ClientData;
import ch.hevs.tb.jackh.smartphysiolibrary.models.PryvPain;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.PryvApi;


public class EventView extends AppCompatActivity {


    private PryvPain mPryvPain;
    private ImageButton plus;
    private ImageButton minus;
    private Button save;
    private Button cancel;
    private TextView painLevel;
    private EditText comment;
    private ClientData mClientData;

    private int min = 0;
    private int max = 10;
    private int current;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_view);

        findById();

        this.mPryvPain = getIntent().getParcelableExtra("pryvEvent");
        if (this.mPryvPain == null) {
            finish();
        }

        getSupportActionBar().setTitle(getString(R.string.title_edit));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        painLevel.setText(mPryvPain.getContent().toString());
        current = Integer.valueOf(mPryvPain.getContent().toString());

        if (current == max) {
            save.setClickable(false);
        }

        if (mPryvPain.getClientData() != null && mPryvPain.getClientData().get(Global.CLIENTDATA_KEY_SPACTIVITY) != null) {
            mClientData = new ClientData(mPryvPain.getClientData());
            comment.setText(mClientData.getComment());


            if (mClientData.isFlag()) {

                //holder.flagImage.setImageResource(R.drawable.ic_flag_48_b);
            }
            if (mClientData.getActivity() != null && !mClientData.getActivity().equals("")) {
                //holder.clientdataActivityNameTextview.setText(mClientData.getActivity());
                //holder.infoImage.setImageResource(R.drawable.ic_running_48_b);
            }
        }


    }

    private void findById() {
        plus = findViewById(R.id.plus_button);
        minus = findViewById(R.id.minus_button);
        save = findViewById(R.id.save_button);
        cancel = findViewById(R.id.cancel_button);
        painLevel = findViewById(R.id.pain_level_textView);
        comment = findViewById(R.id.comment_edittext);
        minus = findViewById(R.id.minus_button);

    }

    public void increasePainLevel(View v) {

        current++;
        painLevel.setText(String.valueOf(current));
        managePlusMinusButton();

    }

    private void managePlusMinusButton() {
        if (current == max) {
            plus.setClickable(false);
        } else {
            plus.setClickable(true);
        }

        if (current == min) {
            minus.setClickable(false);
        } else {
            minus.setClickable(true);
        }

    }

    public void decreasePainLevel(View v) {
        current--;
        painLevel.setText(String.valueOf(current));
        managePlusMinusButton();
    }

    public void save(View v) {

        mPryvPain.setContent(current);

        if (!comment.getText().toString().equals("")) {
            mClientData.setComment(comment.getText().toString());
            mClientData.setFlag(true);
            mPryvPain.addClientData(mClientData);

        }
        PryvApi.updatePain(this, mPryvPain);
        finish();

    }

    public void cancel(View v) {

        finish();

    }

}
