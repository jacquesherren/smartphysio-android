package ch.hevs.tb.jackh.smartphysio_android.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pryv.Connection;
import com.pryv.exceptions.ApiException;
import com.pryv.model.Event;
import com.pryv.model.Filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ch.hevs.tb.jackh.smartphysio_android.EventView;
import ch.hevs.tb.jackh.smartphysio_android.R;
import ch.hevs.tb.jackh.smartphysio_android.adapters.EventListAdapter;
import ch.hevs.tb.jackh.smartphysio_android.adapters.RecyclerItemTouchHelper;
import ch.hevs.tb.jackh.smartphysiolibrary.models.Credentials;
import ch.hevs.tb.jackh.smartphysiolibrary.models.PryvPain;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;

public class FragmentHistory extends Fragment implements EventListAdapter.OnItemClicked, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {


    //private static final String DIALOG_MULTIPLE_CHOICE_CHECK_BOX_LIST = "FragmentHistory.MultipleChoiceCheckBoxListDialog";

    private List<Event> mEventsHistory;
    private EventListAdapter mAdapter;
    private RecyclerView mRecyclerEvents;
    private ConstraintLayout mConstraintLayout;
    private int mSelectedFilters;

    public static FragmentHistory newInstance() {
        FragmentHistory fragment = new FragmentHistory();
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.history_menu, menu);


        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int[] selectedFilter = {1, 2, 3};
        switch (item.getItemId()) {
            case R.id.filter_button_menu:
                //onFilterGroupItemClick(item);
                return true;
            case R.id.flagged:
                mSelectedFilters = 1;
                item.setChecked(true);
                mAdapter.getFilter().filter(String.valueOf(mSelectedFilters));
                return true;
            case R.id.activity:
                mSelectedFilters = 2;
                item.setChecked(true);
                mAdapter.getFilter().filter(String.valueOf(mSelectedFilters));
                return true;
            case R.id.all:
                mSelectedFilters = 3;
                item.setChecked(true);
                mAdapter.getFilter().filter("");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();

        retrievePains(this.getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.history_events_fragment, container, false);

        mConstraintLayout = view.findViewById(R.id.container);
        mRecyclerEvents = view.findViewById(R.id.events_recycler);

        // set RecyclerView
        mRecyclerEvents.setHasFixedSize(true);
        mRecyclerEvents.setLayoutManager(new LinearLayoutManager(this.getContext()));
        mRecyclerEvents.setItemAnimator(new DefaultItemAnimator());
        mRecyclerEvents.addItemDecoration(new DividerItemDecoration(this.getContext(), DividerItemDecoration.VERTICAL));
        mEventsHistory = new ArrayList<>();

        mAdapter = new EventListAdapter(mEventsHistory, this.getContext());
        //retrievePains(this);

        mRecyclerEvents.setAdapter(mAdapter);
        mAdapter.setOnClick(this); // Bind the listener

        setHasOptionsMenu(true);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerEvents);

        return view;
    }

    @Override
    public void onItemClick(int position) {
        Toast.makeText(this.getContext(), "position : " + position + mEventsHistory.get(position).getContent().toString(), Toast.LENGTH_SHORT);

        Event event = mEventsHistory.get(position);
        PryvPain pryvPain = new PryvPain(event);

        Intent i = new Intent(this.getActivity(), EventView.class);
        i.putExtra("pryvEvent", pryvPain);
        startActivity(i);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof EventListAdapter.MyViewHolder) {
            // get the removed item name to display it in snack bar
            String name = mEventsHistory.get(viewHolder.getAdapterPosition()).getContent().toString();

            // backup of removed item for undo purpose
            final Event deletedItem = mEventsHistory.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            mAdapter.removeItem(viewHolder.getAdapterPosition());

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(mConstraintLayout, "Removed from history!", Snackbar.LENGTH_LONG);

            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    mAdapter.restoreItem(deletedItem, deletedIndex);

                }
            });

            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();

            Toast.makeText(this.getContext(), "deleted", Toast.LENGTH_SHORT).show();

        }
    }

    public void retrievePains(final Context context) {
        Credentials credentials = new Credentials(context);
        if (credentials.hasCredentials()) {
            final Connection connection = new Connection(credentials.getUsername(), credentials.getToken(), Global.DOMAIN);
            new Thread() {
                public void run() {
                    try {
                        Filter filter = new Filter().addStream(Global.PAIN_STREAM);
                        filter.setLimit(0);
                        List<Event> events = connection.events.get(filter);


                        updateEventsList(events);
                    } catch (IOException e) {
                        //updateStatusText(context, e.toString());
                    } catch (ApiException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    public void updateEventsList(final List<Event> events) {
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mEventsHistory.clear();
                mEventsHistory.addAll(events);
                mEventsHistory.sort(new Comparator<Event>() {
                    @Override
                    public int compare(Event e1, Event e2) {
                        //return e1.getCreated() > e2.getCreated() ? 1 :(e1.getCreated() < e2.getCreated() ? -1 : 0); //Sorted in Ascending
                        return e1.getTime() > e2.getTime() ? -1 : (e1.getTime() < e2.getTime() ? 1 : 0); // Descending
                    }
                });
                mAdapter.notifyDataSetChanged();
            }
        });
    }

}
