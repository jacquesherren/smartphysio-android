package ch.hevs.tb.jackh.smartphysio_android.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ch.hevs.tb.jackh.smartphysio_android.R;

public class FragmentAbout extends Fragment {

    public static FragmentAbout newInstance() {
        FragmentAbout fragment = new FragmentAbout();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about_events_fragment, container, false);
    }

}
