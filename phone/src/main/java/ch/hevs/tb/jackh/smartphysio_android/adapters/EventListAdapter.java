package ch.hevs.tb.jackh.smartphysio_android.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pryv.model.Event;

import java.util.ArrayList;
import java.util.List;

import ch.hevs.tb.jackh.smartphysio_android.R;
import ch.hevs.tb.jackh.smartphysiolibrary.models.ClientData;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;
import ch.hevs.tb.jackh.smartphysiolibrary.utils.PryvApi;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.MyViewHolder> implements Filterable {

    private MyFilter mFilter;

    private List<Event> mEvents;
    private List<Event> mFilteredEvents;
    private Context mContext;

    private OnItemClicked onClick;

    public EventListAdapter(List<Event> mEvents, Context mContext) {
        this.mEvents = mEvents;
        this.mFilteredEvents = new ArrayList<>();
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public EventListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_list_adapter, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull EventListAdapter.MyViewHolder holder, final int position) {
        final ClientData cd;
        if (mEvents != null && !mEvents.isEmpty()) {

            Event event = mEvents.get(position);

            holder.infoImage.setImageResource(0);
            holder.flagImage.setImageResource(0);
            holder.clientdataCommentTextview.setText("");
            holder.clientdataActivityNameTextview.setText("");
            Double dCreated = new Double(event.getTime());
            long created = dCreated.longValue();
            holder.mPainCreatedTextView.setText(Global.getTimeAgo(created, mContext));
            holder.mPainLevelTextView.setText(event.getContent().toString());


            if (event.getClientData() != null && event.getClientData().get(Global.CLIENTDATA_KEY_SPACTIVITY) != null) {
                cd = new ClientData(event.getClientData());
                if (cd.isFlag()) {
                    holder.clientdataCommentTextview.setText(cd.getComment());
                    holder.flagImage.setImageResource(R.drawable.ic_flag_48_b);
                }
                if (cd.getActivity() != null && !cd.getActivity().equals("")) {
                    holder.clientdataActivityNameTextview.setText(cd.getActivity());
                    holder.infoImage.setImageResource(R.drawable.ic_running_48_b);
                }
            }
            holder.layout.setOnClickListener(new View.OnClickListener() {
                                                 public void onClick(View v) {
                                                     onClick.onItemClick(position);
                                                 }
                                             }

            );
            int painValue = Integer.valueOf(event.getContent().toString());
            switch (painValue) {
                case 0:
                    holder.mPainLevelTextView.setBackgroundResource(R.drawable.ic_circle_0);
                    break;
                case 1:
                case 2:
                case 3:
                    holder.mPainLevelTextView.setBackgroundResource(R.drawable.ic_circle_123);
                    break;
                case 4:
                case 5:
                case 6:
                    holder.mPainLevelTextView.setBackgroundResource(R.drawable.ic_circle_456);
                    break;
                case 7:
                case 8:
                case 9:
                    holder.mPainLevelTextView.setBackgroundResource(R.drawable.ic_circle_789);
                    break;
                case 10:
                    holder.mPainLevelTextView.setBackgroundResource(R.drawable.ic_circle_10);
                    break;
            }


        }
    }

    @Override
    public int getItemCount() {
        if (mEvents != null && !mEvents.isEmpty()) {
            return mEvents.size();
        }
        return 0;
    }

    public void setOnClick(OnItemClicked onClick) {
        this.onClick = onClick;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilteredEvents.clear();
            mFilteredEvents.addAll(this.mEvents);
            mFilter = new EventListAdapter.MyFilter(this, mFilteredEvents);
        }
        return mFilter;
    }

    //make interface like this
    public interface OnItemClicked {
        void onItemClick(int position);
    }


    public void removeItem(int position) {
        Event event = mEvents.get(position);
        mEvents.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()

        PryvApi.deletePain(mContext, event);

        notifyItemRemoved(position);
    }

    public void restoreItem(Event event, int position) {
        mEvents.add(position, event);
        // notify item added by position

        PryvApi.restorePain(mContext, event);

        notifyItemInserted(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView mPainLevelTextView;
        private final TextView mPainCreatedTextView;
        private final TextView clientdataCommentTextview;
        private final TextView clientdataActivityNameTextview;
        private final ImageView infoImage;
        private final ImageView flagImage;
        private final ConstraintLayout layout;

        public RelativeLayout viewBackground;
        public ConstraintLayout viewForeground;


        public MyViewHolder(View itemView) {
            super(itemView);
            mPainLevelTextView = itemView.findViewById(R.id.pain_level_textView);
            mPainCreatedTextView = itemView.findViewById(R.id.pain_timestamp_textview);
            clientdataCommentTextview = itemView.findViewById(R.id.clientdata_comment_textview);
            clientdataActivityNameTextview = itemView.findViewById(R.id.clientdata_activity_name_textview);
            infoImage = itemView.findViewById(R.id.info_image);
            flagImage = itemView.findViewById(R.id.flag_image);
            layout = itemView.findViewById(R.id.event_list_adapter_layout);

            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);


        }
    }

    private class MyFilter extends Filter {

        private final EventListAdapter mEventAdapter;
        private final List<Event> mOriginalList;
        private final List<Event> mFilteredList;

        private MyFilter(EventListAdapter eventAdapter, List<Event> originalList) {
            this.mEventAdapter = eventAdapter;
            this.mOriginalList = originalList;
            this.mFilteredList = new ArrayList<Event>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            mFilteredList.clear();
            final FilterResults results = new FilterResults();
            if (charSequence.length() == 0) {
                mFilteredList.addAll(mOriginalList);
            } else {
                final int res = Integer.valueOf(charSequence.toString());
                ClientData cd;
                for (Event event : mOriginalList) {

                    if (event.getClientData() != null && event.getClientData().get(Global.CLIENTDATA_KEY_SPACTIVITY) != null) {
                        cd = new ClientData(event.getClientData());
                        switch (res) {
                            case 1:
                                if (cd.isFlag()) {
                                    mFilteredList.add(event);
                                }
                                break;

                            case 2:
                                if (cd.getActivity() != null && !cd.getActivity().equals("")) {
                                    mFilteredList.add(event);
                                }
                                break;

                            case 3:
                                if (cd.isFlag()) {
                                    mFilteredList.add(event);
                                }
                                if (cd.getActivity() != null && !cd.getActivity().equals("")) {
                                    mFilteredList.add(event);
                                }
                                break;

                        }
                    }
                }
            }

            results.values = mFilteredList;
            results.count = mFilteredList.size();
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mEventAdapter.mEvents.clear();
            mEventAdapter.mEvents.addAll((ArrayList<Event>) results.values);
            mEventAdapter.notifyDataSetChanged();
        }
    }
}