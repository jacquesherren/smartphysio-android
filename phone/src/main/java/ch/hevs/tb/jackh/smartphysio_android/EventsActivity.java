package ch.hevs.tb.jackh.smartphysio_android;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import ch.hevs.tb.jackh.smartphysio_android.fragments.FragmentAbout;
import ch.hevs.tb.jackh.smartphysio_android.fragments.FragmentChart;
import ch.hevs.tb.jackh.smartphysio_android.fragments.FragmentHistory;
import ch.hevs.tb.jackh.smartphysiolibrary.models.Credentials;

public class EventsActivity extends AppCompatActivity {

    private Credentials credentials;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_history:
                    getSupportActionBar().setTitle(getString(R.string.title_history));
                    selectedFragment = FragmentHistory.newInstance();
                    break;
                case R.id.navigation_chart:
                    getSupportActionBar().setTitle(getString(R.string.title_chart));
                    selectedFragment = FragmentChart.newInstance();
                    break;
                case R.id.navigation_about:
                    getSupportActionBar().setTitle(getString(R.string.title_about));
                    selectedFragment = FragmentAbout.newInstance();
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, selectedFragment);
            transaction.commit();
            return true;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        //Toolbar toolbar = findViewById(R.id.toolbar);
        getSupportActionBar().setTitle(getString(R.string.title_history));


        //mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //Manually displaying the first fragment - one time only
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, FragmentHistory.newInstance());
        transaction.commit();


    }


    @Override
    protected void onStart() {
        super.onStart();

        credentials = new Credentials(this);

        if (!credentials.hasCredentials()) {
            finish();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out_button_menu:
                signOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void signOut() {
        credentials.resetCredentials();
        finish();
    }

}
