package ch.hevs.tb.jackh.smartphysiolibrary.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.pryv.model.Event;

import java.util.HashMap;

import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;

public class PryvPain extends PryvEvent {

    private int content;
    private int color;

    public PryvPain(Event e) {
        this.content = Integer.valueOf(e.getContent().toString());

        super.setId(e.getId());
        super.setTime(e.getTime());
        super.setStreamId(e.getStreamId());
        super.setType(e.getType());
        super.setCreated(e.getCreated());
        super.setCreatedBy(e.getCreatedBy());
        super.setModified(e.getModified());
        super.setModifiedBy(e.getModifiedBy());
        super.setClientData(e.getClientData());

    }


    public static final Parcelable.Creator<PryvPain> CREATOR = new Parcelable.Creator<PryvPain>() {
        @Override
        public PryvPain createFromParcel(Parcel source) {
            return new PryvPain(source);
        }

        @Override
        public PryvPain[] newArray(int size) {
            return new PryvPain[size];
        }
    };

    public Object getContent() {
        return content;
    }

    public void setContent(int content) {
        this.content = content;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public PryvPain(int content) {
        super(Global.PAIN_STREAM_ID, Global.PAIN_EVENT_TYPE);
        this.content = content;

    }

    /**
     * Retrieving Movie data from Parcel object
     * This constructor is invoked by the method createFromParcel(Parcel source) of
     * the object CREATOR
     **/
    private PryvPain(Parcel in) {
        this.content = in.readInt();
        this.color = in.readInt();

        super.setId(in.readString());
        super.setTime(in.readDouble());
        super.setStreamId(in.readString());
        super.setType(in.readString());
        super.setCreated(in.readDouble());
        super.setCreatedBy(in.readString());
        super.setModified(in.readDouble());
        super.setModifiedBy(in.readString());

        super.setClientData(in.readHashMap(HashMap.class.getClassLoader()));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(content);
        dest.writeInt(color);
        dest.writeString(super.getId());
        dest.writeDouble(super.getTime());
        dest.writeString(super.getStreamId());
        dest.writeString(super.getType());
        dest.writeDouble(super.getCreated());
        dest.writeString(super.getCreatedBy());
        if (super.getModified() != null)
            dest.writeDouble(super.getModified());
        if (super.getModifiedBy() != null)
            dest.writeString(super.getModifiedBy());
        dest.writeMap(super.getClientData());
    }
}
