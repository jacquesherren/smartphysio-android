package ch.hevs.tb.jackh.smartphysiolibrary.models;

import android.os.Parcelable;

import com.pryv.model.Event;

import java.util.HashMap;
import java.util.Map;

import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;

public abstract class PryvEvent extends Event implements Parcelable {


    public PryvEvent() {
        super();

    }

    ;

    public PryvEvent(String streamId, String type){
        super();
        super.setStreamId(streamId);
        super.setType(type);
        super.setCreated(setDate());
        super.setTime(super.getCreated());
    }


    public void addClientData(ClientData clientData) {
        Map<String, Object> cd = new HashMap<>();
        cd.put(Global.CLIENTDATA_KEY_SPACTIVITY, clientData);
        super.setClientData(cd);
    }

    private Double setDate() {
        Double date = ((double) (System.currentTimeMillis())) / 1000.0;
        return date;
    }
}
