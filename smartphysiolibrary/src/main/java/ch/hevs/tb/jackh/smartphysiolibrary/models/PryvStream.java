package ch.hevs.tb.jackh.smartphysiolibrary.models;

import com.pryv.model.Event;
import com.pryv.model.Stream;

import java.util.ArrayList;
import java.util.List;

public class PryvStream extends Stream {


    List<Event> events;

    public PryvStream(String id, String name) {
        super(id, name);
    }

    public PryvStream(Stream s) {
        super(s.getId(), s.getName());
    }

    public void addEvent(Event event) {
        if (events == null) {
            events = new ArrayList<>();
        }
        events.add(event);
    }


    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public void clearEvents() {

        if (this.events != null && this.events.size() > 0)
            this.events.clear();

    }



}
