package ch.hevs.tb.jackh.smartphysiolibrary.models;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.io.Serializable;
import java.util.Map;

import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;

public class ClientData implements Serializable {


    private String activity;
    private String id;
    private String streamId;
    private boolean flag;
    private String comment;


    public ClientData() {
        super();
        this.activity = "";
        this.id = "";
        this.streamId = "";
        this.flag = false;
        this.comment = "";
    }


    public ClientData(Map<String, Object> cd) {
        Gson gson = new Gson();
        JsonElement jsonElement = gson.toJsonTree(cd.get(Global.CLIENTDATA_KEY_SPACTIVITY));
        ClientData newClientData = gson.fromJson(jsonElement, ClientData.class);

        newClientData.toString();
        this.flag = newClientData.isFlag();
        this.activity = newClientData.activity;
        this.comment = newClientData.comment;
        this.streamId = newClientData.streamId;
        this.id = newClientData.id;

    }
    public ClientData(String id, Object content, String streamId) {
        super();
        this.activity = content.toString();
        this.id = id;
        this.streamId = streamId;
        this.flag = false;
    }

    public ClientData(String activity, boolean flag) {
        super();
        this.activity = activity;
        this.flag = flag;
    }

    public ClientData(String activity) {
        this.activity = activity;
    }

    public ClientData(boolean flag) {
        this.flag = flag;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String content) {
        this.activity = activity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "ClientData{" +
                "activity='" + activity + '\'' +
                ", id='" + id + '\'' +
                ", streamId='" + streamId + '\'' +
                ", flag=" + flag +
                ", comment='" + comment + '\'' +
                '}';
    }
}
