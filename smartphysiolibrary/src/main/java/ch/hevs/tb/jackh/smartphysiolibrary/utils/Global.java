package ch.hevs.tb.jackh.smartphysiolibrary.utils;

import android.content.Context;

import com.pryv.model.Event;
import com.pryv.model.Stream;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import ch.hevs.tb.jackh.smartphysiolibrary.BuildConfig;
import ch.hevs.tb.jackh.smartphysiolibrary.models.PryvStream;

public class Global {


    /**
     * Set Domain and route API
     */
    public static final String DOMAIN_URL_ROUTE_API = ".pryv.me/";
    public static final String BASE_URL_ROUTE_API = "https://";
    public static final String PRYV_URL_API = "{username}" + DOMAIN_URL_ROUTE_API;

    public static final String CLIENTDATA_KEY_SPACTIVITY = "spActivity";


    public final static String DOMAIN = "pryv.me";
    public final static String APPID = "smartphysio_android";

    public static final int PORT = 15002;
    public static final int TIMEOUT = 30 * 1000;

    public static final String PAIN_STREAM_PARENT_ID = "smartphysio";
    public static final String PAIN_STREAM_PARENT_NAME = "Smart Physio";


    public static final String PAIN_STREAM_ID = "levelPains";
    public static final String PAIN_STREAM_NAME = "Level pains";
    public static final String PAIN_EVENT_TYPE = "count/generic";

    public static final String PUBLIC_ACTIVITY_USERNAME = "spadmin";
    public static final String PUBLIC_ACTIVITY_AUTH = "cjj6vct480q6y0cd35r5fwon7";

    public static final PryvStream FAVORITES_STREAM = new PryvStream("favorites", "Favorites");
    public static final Stream PAIN_STREAM = new Stream(Global.PAIN_STREAM_ID, Global.PAIN_STREAM_NAME);

    public static List<Event> activitiesOnline;
    public static List<PryvStream> types;
    private static final int SECOND_MILLIS = 1000;


    public static boolean isDEBUG() {
        return (BuildConfig.DEBUG);
    }

    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    public static List<Event> eventsHistory;

    public static String getTimeAgo(long time, Context ctx) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }
        SimpleDateFormat allDate = new SimpleDateFormat("EEE, dd MMM yyyy");
        SimpleDateFormat hhmm = new SimpleDateFormat("hh:mm");

        long now = Calendar.getInstance().getTimeInMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday at " + hhmm.format(time);
        } else if (diff < 24 * DAY_MILLIS) {
            return diff / DAY_MILLIS + " days ago at " + hhmm.format(time);
        } else {
            return allDate.format(time) + " " + hhmm.format(time);

        }
    }
}

