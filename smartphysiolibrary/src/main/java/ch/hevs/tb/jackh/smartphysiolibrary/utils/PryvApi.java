package ch.hevs.tb.jackh.smartphysiolibrary.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.pryv.Connection;
import com.pryv.exceptions.ApiException;
import com.pryv.model.Event;
import com.pryv.model.Filter;
import com.pryv.model.Stream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ch.hevs.tb.jackh.smartphysiolibrary.models.Credentials;
import ch.hevs.tb.jackh.smartphysiolibrary.models.PryvPain;
import ch.hevs.tb.jackh.smartphysiolibrary.models.PryvStream;

public class PryvApi {

    private static final String TAG = PryvApi.class.getName() + "*** JACKH ***";
    private static Connection connection;
    private static Stream painStream;
    private static Stream smartPhysioStream;


    /**
     * Manage Pryv connexion
     */


    public static void initPryvConnection(final Context context, Credentials credentials) {


        // Initiate new connection to Pryv with connected account
        connection = new Connection(credentials.getUsername(), credentials.getToken(), Global.DOMAIN);

        // Initiate a "Pains" stream containing notes if not already created
        painStream = new Stream()
                .setId(Global.PAIN_STREAM_ID)
                .setName(Global.PAIN_STREAM_NAME);

        smartPhysioStream = new Stream()
                .setId(Global.PAIN_STREAM_PARENT_ID)
                .setName(Global.PAIN_STREAM_PARENT_NAME)
                .addChildStream(painStream);


        new Thread() {
            public void run() {
                try {

                    Stream createdSmartPhysioStream = connection.streams.create(smartPhysioStream);
                    updateStatusText(context, "Stream created with id: " + createdSmartPhysioStream.getId());

                    Log.i(TAG, "010 / " + createdSmartPhysioStream.toString());
                } catch (IOException ioException) {

                    updateStatusText(context, ioException.toString());
                } catch (ApiException apiException) {
                    if ((apiException.getId()).equals("item-already-exists")) {
                        // Do nothing stream already exist
                        Log.i(TAG, "streams already exists !");
                    } else {
                        updateStatusText(context, apiException.toString());
                    }
                }

                try {

                    Stream createdPainStream = connection.streams.create(painStream);
                    updateStatusText(context, "Stream created with id: " + createdPainStream.getId());

                    Log.i(TAG, "010 / " + createdPainStream.toString());
                } catch (IOException ioException) {

                    updateStatusText(context, ioException.toString());
                } catch (ApiException apiException) {
                    if ((apiException.getId()).equals("item-already-exists")) {
                        // Do nothing stream already exist
                        Log.i(TAG, "streams already exists !");
                    } else {
                        updateStatusText(context, apiException.toString());
                    }
                }


            }
        }.start();
    }

    public static void retrievePains(final Context context) {
        Credentials credentials = new Credentials(context);
        if (credentials.hasCredentials()) {
            connection = new Connection(credentials.getUsername(), credentials.getToken(), Global.DOMAIN);
            new Thread() {
                public void run() {
                    try {
                        Filter filter = new Filter().addStream(Global.PAIN_STREAM);
                        filter.setLimit(0);
                        List<Event> events = connection.events.get(filter);
                        //updateStatusText(context, "Pains history retrieved");// + newEvent.getId());
                        updateEventsList(events);
                    } catch (IOException e) {
                        //updateStatusText(context, e.toString());
                    } catch (ApiException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    public static void updateEventsList(final List<Event> events) {
        System.out.println("************   SIZE : " + events.size());
        Global.eventsHistory = new ArrayList<Event>(events);
    }


    private static void updateStatusText(final Context context, final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.w(TAG, "*-*-*-**-*-*-*-*-*-*-* 900 / " + text);
                Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private static void runOnUiThread(Runnable runnable) {

        //System.out.println("Runnable : " + runnable);

    }

    public static void getPublicActivitiesOnPryv(final Context context) {

        // Initiate new connection to Pryv with connected account
        final Connection publicConnection = new Connection(Global.PUBLIC_ACTIVITY_USERNAME, Global.PUBLIC_ACTIVITY_AUTH, Global.DOMAIN);

        new Thread() {
            public void run() {
                try {
                    // get all public activities
                    List<Event> publicActivitiesOnPryv;
                    Filter filter = new Filter().setParentId("activities");

                    //limit = 0 > get all
                    filter.setLimit(0);
                    publicActivitiesOnPryv = publicConnection.events.get(filter);
                    if (publicActivitiesOnPryv.size() > 0) {
                        //^set global variable to keep it
                        Global.activitiesOnline = new ArrayList<>(publicActivitiesOnPryv);
                    }

                } catch (IOException | ApiException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static void getPublicActivitiesTypesOnPryv(final Context context) {


        // Initiate new connection to Pryv with connected account
        final Connection publicConnection = new Connection(Global.PUBLIC_ACTIVITY_USERNAME, Global.PUBLIC_ACTIVITY_AUTH, Global.DOMAIN);

        new Thread() {
            public void run() {
                try {

                    Map<String, Stream> publicActivitiesTypesOnPryv;
                    Filter filter = new Filter().setParentId("activities");
                    filter.setLimit(0);
                    publicActivitiesTypesOnPryv = publicConnection.streams.get(filter);
                    if (publicActivitiesTypesOnPryv.size() > 0) {
                        Global.types = new ArrayList<>();
                        Global.types.add(Global.FAVORITES_STREAM);
                        for (String key : publicActivitiesTypesOnPryv.keySet()) {
                            System.out.println("map.get = " + publicActivitiesTypesOnPryv.get(key));
                            Global.types.add(new PryvStream(publicActivitiesTypesOnPryv.get(key)));
                        }

                    }


                } catch (IOException | ApiException e) {
                    e.printStackTrace();
                }


            }
        }.start();
    }

    public static void addPain(final Context context, final PryvPain pryvPain) {
        Credentials credentials = new Credentials(context);
        if (credentials.hasCredentials()) {
            connection = new Connection(credentials.getUsername(), credentials.getToken(), Global.DOMAIN);
            if (pryvPain != null) {
                new Thread() {
                    public void run() {
                        try {
                            Event newEvent = new Event()
                                    .setStreamId(pryvPain.getStreamId())
                                    .setType(pryvPain.getType())
                                    .setContent(pryvPain.getContent())
                                    .setCreated(pryvPain.getCreated())
                                    .setTime(pryvPain.getTime())
                                    .setClientData(pryvPain.getClientData());
                            newEvent = connection.events.create(newEvent);
                            updateStatusText(context, "Saved");// + newEvent.getId());
                        } catch (IOException | ApiException e) {
                            updateStatusText(context, e.toString());
                        }
                    }
                }.start();
            }
        }
    }

    public static void deletePain(final Context context, final Event pryvPain) {
        Credentials credentials = new Credentials(context);
        if (credentials.hasCredentials()) {
            connection = new Connection(credentials.getUsername(), credentials.getToken(), Global.DOMAIN);
            if (pryvPain != null) {
                new Thread() {
                    public void run() {
                        try {
                            Event deletingEvent = new Event()
                                    .setId(pryvPain.getId())
                                    .setStreamId(pryvPain.getStreamId())
                                    .setType(pryvPain.getType())
                                    .setContent(pryvPain.getContent())
                                    .setCreated(pryvPain.getCreated())
                                    .setTime(pryvPain.getTime())
                                    .setClientData(pryvPain.getClientData())
                                    .setCreatedBy(pryvPain.getCreatedBy())
                                    .setDate(pryvPain.getDate())
                                    .setDeleted(pryvPain.isDeleted())
                                    .setDescription(pryvPain.getDescription())
                                    .setDuration(pryvPain.getDuration())
                                    .setModified(pryvPain.getModified())
                                    .setModifiedBy(pryvPain.getModifiedBy())
                                    .setTags(pryvPain.getTags())
                                    .setTrashed(pryvPain.isTrashed())
                                    .setType(pryvPain.getType());
                            Event deletedEvent = connection.events.delete(deletingEvent);
                            updateStatusText(context, "Deleted");// + newEvent.getId());
                        } catch (IOException | ApiException e) {
                            updateStatusText(context, e.toString());
                        }
                    }
                }.start();
            }
        }
    }

    public static void restorePain(final Context context, final Event pryvPain) {
        Credentials credentials = new Credentials(context);
        if (credentials.hasCredentials()) {
            connection = new Connection(credentials.getUsername(), credentials.getToken(), Global.DOMAIN);
            if (pryvPain != null) {
                new Thread() {
                    public void run() {
                        try {
                            Event newEvent = new Event()
                                    .setId(pryvPain.getId())
                                    .setStreamId(pryvPain.getStreamId())
                                    .setType(pryvPain.getType())
                                    .setContent(pryvPain.getContent())
                                    .setCreated(pryvPain.getCreated())
                                    .setTime(pryvPain.getTime())
                                    .setClientData(pryvPain.getClientData())
                                    .setCreatedBy(pryvPain.getCreatedBy())
                                    .setDate(pryvPain.getDate())
                                    .setDeleted(pryvPain.isDeleted())
                                    .setDescription(pryvPain.getDescription())
                                    .setDuration(pryvPain.getDuration())
                                    .setModified(pryvPain.getModified())
                                    .setModifiedBy(pryvPain.getModifiedBy())
                                    .setTags(pryvPain.getTags())
                                    .setTrashed(false)
                                    .setType(pryvPain.getType());
                            Event restoredEvent = connection.events.update(newEvent);
                            updateStatusText(context, "Updated");// + newEvent.getId());
                        } catch (IOException | ApiException e) {
                            updateStatusText(context, e.toString());
                        }
                    }
                }.start();
            }
        }
    }

    public static void updatePain(final Context context, final PryvPain pryvPain) {
        Credentials credentials = new Credentials(context);
        if (credentials.hasCredentials()) {
            connection = new Connection(credentials.getUsername(), credentials.getToken(), Global.DOMAIN);
            if (pryvPain != null) {
                new Thread() {
                    public void run() {
                        try {
                            Event newEvent = new Event()
                                    .setId(pryvPain.getId())
                                    .setStreamId(pryvPain.getStreamId())
                                    .setType(pryvPain.getType())
                                    .setContent(pryvPain.getContent())
                                    .setCreated(pryvPain.getCreated())
                                    .setTime(pryvPain.getTime())
                                    .setClientData(pryvPain.getClientData())
                                    .setCreatedBy(pryvPain.getCreatedBy())
                                    .setDate(pryvPain.getDate())
                                    .setDeleted(pryvPain.isDeleted())
                                    .setDescription(pryvPain.getDescription())
                                    .setDuration(pryvPain.getDuration())
                                    .setModified(pryvPain.getModified())
                                    .setModifiedBy(pryvPain.getModifiedBy())
                                    .setTags(pryvPain.getTags())
                                    .setTrashed(pryvPain.isTrashed())
                                    .setType(pryvPain.getType());
                            Event updatedEvent = connection.events.update(newEvent);
                            updateStatusText(context, "Updated");// + newEvent.getId());
                        } catch (IOException | ApiException e) {
                            updateStatusText(context, e.toString());
                        }
                    }
                }.start();
            }
        }
    }

}
