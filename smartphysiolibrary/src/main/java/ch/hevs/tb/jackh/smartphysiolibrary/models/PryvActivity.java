package ch.hevs.tb.jackh.smartphysiolibrary.models;

import android.os.Parcel;
import android.os.Parcelable;

public class PryvActivity extends PryvEvent {

    public static final Parcelable.Creator<PryvActivity> CREATOR = new Parcelable.Creator<PryvActivity>() {
        @Override
        public PryvActivity createFromParcel(Parcel source) {
            return new PryvActivity(source);
        }

        @Override
        public PryvActivity[] newArray(int size) {
            return new PryvActivity[size];
        }
    };

    public PryvActivity(String id, Object content, String streamId) {
        super();
        super.setContent(content);
        super.setId(id);
        super.setStreamId(streamId);
    }

    private PryvActivity(Parcel in) {
        super.setContent(in.readInt());
        super.setId(in.readString());
        super.setTime(in.readDouble());
        super.setStreamId(in.readString());
        super.setType(in.readString());
        super.setCreated(in.readDouble());
        super.setCreatedBy(in.readString());
        super.setModified(in.readDouble());
        super.setModifiedBy(in.readString());
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(super.getContent().toString());
        dest.writeString(super.getId());
        dest.writeDouble(super.getTime());
        dest.writeString(super.getStreamId());
        dest.writeString(super.getType());
        dest.writeDouble(super.getCreated());
        dest.writeString(super.getCreatedBy());
        if (super.getModified() != null)
            dest.writeDouble(super.getModified());
        if (super.getModifiedBy() != null)
            dest.writeString(super.getModifiedBy());
    }
}
