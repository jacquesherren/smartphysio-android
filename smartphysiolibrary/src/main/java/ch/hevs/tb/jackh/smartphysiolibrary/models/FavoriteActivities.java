package ch.hevs.tb.jackh.smartphysiolibrary.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import ch.hevs.tb.jackh.smartphysiolibrary.utils.Global;

public class FavoriteActivities {

    private final static String SHARED_PREFERENCES_FILE_CLIENT_DATA_LIST = "smartphysioClientData";
    private final static String SHARED_PREFERENCES_FAVORITES = "smartphysioClientDataFavorite";


    private SharedPreferences preferences;

    public static void resetFavorites(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(SHARED_PREFERENCES_FILE_CLIENT_DATA_LIST, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.remove(SHARED_PREFERENCES_FAVORITES);
        editor.apply();
    }


    public static void storeClientData(Context context, Map<String, ClientData> favorites) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(SHARED_PREFERENCES_FILE_CLIENT_DATA_LIST, Context.MODE_PRIVATE);
        editor = settings.edit();

        for (String key : favorites.keySet()) {
            favorites.get(key).setStreamId(Global.FAVORITES_STREAM.getId());
        }

        // Map to JSON
        Gson gson = new Gson(); // com.google.gson.Gson
        String jsonFavorites = gson.toJson(favorites);
        System.out.println(jsonFavorites); // {"1": "object"}

        editor.putString(SHARED_PREFERENCES_FAVORITES, jsonFavorites);
        editor.apply();

    }

    public static Map<String, ClientData> loadFavorites(Context context) {
        // used for retrieving arraylist from json formatted string
        SharedPreferences settings;
        Map<String, ClientData> favorites;

        settings = context.getSharedPreferences(SHARED_PREFERENCES_FILE_CLIENT_DATA_LIST, Context.MODE_PRIVATE);

        //if patient has favorites
        if (settings.contains(SHARED_PREFERENCES_FAVORITES)) {
            String jsonFavorites = settings.getString(SHARED_PREFERENCES_FAVORITES, null);

            // JSON to Map
            Gson gson = new Gson(); // com.google.gson.Gson
            Type type = new TypeToken<Map<String, ClientData>>() {
            }.getType();
            favorites = gson.fromJson(jsonFavorites, type);
            for (String key : favorites.keySet()) {
                System.out.println("map.get = " + favorites.get(key));
            }
            return favorites;

        }

        return null;

    }

    public static void addFavorite(Context context, ClientData clientData) {
        Map<String, ClientData> favorites = loadFavorites(context);
        if (favorites == null)
            favorites = new HashMap<>();
        favorites.put(clientData.getId(), clientData);
        storeClientData(context, favorites);
    }

    public static void removeFavorite(Context context, ClientData clientData) {
        Map<String, ClientData> favorites = loadFavorites(context);
        if (favorites != null) {
            favorites.remove(clientData.getId());
            storeClientData(context, favorites);
        }
    }

}
